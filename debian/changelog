liblist-someutils-perl (0.59-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 01 Apr 2023 03:01:26 +0000

liblist-someutils-perl (0.59-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.59.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Sat, 31 Dec 2022 18:23:43 +0100

liblist-someutils-perl (0.58-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 15 Oct 2022 20:05:30 +0100

liblist-someutils-perl (0.58-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 14 Oct 2022 15:20:50 +0100

liblist-someutils-perl (0.58-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 12 Mar 2021 00:00:18 +0000

liblist-someutils-perl (0.58-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Nick Morrott ]
  * New upstream version 0.58
  * d/control:
    - Declare compliance with Debian Policy 4.4.1
    - Bump debhelper-compat level to 12
    - Add Rules-Requires-Root field
    - Annotate test-only build dependencies with <!nocheck>
    - Add recommended dependency on liblist-someutils-xs-perl
      (Ignore at in B-D so that we only build test ::PP implementation)
  * d/copyright:
    - Refresh years of upstream copyright
    - Refresh years of Debian copyright
  * d/docs:
    - Install upstream Code of Conduct
    - Drop upstream README.md
  * d/NEWS:
    - Add details of backwards incompatibilities
  * d/u/metadata:
    - Add Bug-Submit field; drop deprecated Contact, Name fields
  * d/watch:
    - Migrate to version 4 watch file format

 -- Nick Morrott <nickm@debian.org>  Wed, 11 Dec 2019 02:14:30 +0000

liblist-someutils-perl (0.56-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 01:13:23 +0000

liblist-someutils-perl (0.56-1) unstable; urgency=medium

  * Import upstream version 0.56

 -- Lucas Kanashiro <kanashiro@debian.org>  Tue, 08 Aug 2017 18:50:58 -0300

liblist-someutils-perl (0.54-1) unstable; urgency=medium

  * Import upstream version 0.54
  * Update upstream metadata
  * Update years of upstream copyright
  * Update years of Debian packaging copyright
  * Bump debhelper compatibility level to 10
  * Declare compliance with Debian policy 4.0.0

 -- Lucas Kanashiro <kanashiro@debian.org>  Wed, 21 Jun 2017 16:59:42 -0300

liblist-someutils-perl (0.53-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.53.
  * Remove (build) dependency on libexporter-tiny-perl.

 -- gregor herrmann <gregoa@debian.org>  Mon, 29 Aug 2016 22:37:30 +0200

liblist-someutils-perl (0.52-1) unstable; urgency=low

  * Initial Release. (Closes: #818031)

 -- Lucas Kanashiro <kanashiro@debian.org>  Sun, 03 Jul 2016 13:49:45 -0300
